object Versions {
    //kotlin版本
    const val KotlinVersion = "1.4.21"

    //SDK版本
    const val compileSdkVersion = 30
    const val buildToolsVersion = "30.0.3"
    const val minSdkVersion = 22
    const val targetSdkVersion = 30

    //依赖
    const val retrofit = "com.squareup.retrofit2:retrofit:2.9.0"
    const val retrofit_converter_gson = "com.squareup.retrofit2:converter-gson:2.9.0"
    const val retrofit_logging_interceptor = "com.squareup.okhttp3:logging-interceptor:4.7.2"
}